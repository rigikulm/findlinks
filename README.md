# findlinks

Findlinks prints the links in an HTML document read from standard input.

    This code is based on the similarly named program from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

## Sample Programs

### findlinks usage

```bash
fetch <url> | findlinks
```
